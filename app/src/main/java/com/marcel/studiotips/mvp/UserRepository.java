package com.marcel.studiotips.mvp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


/**
 * Created by marcel on 28.1.2016.
 */
public class UserRepository {

	public interface ResultHandler<T> {
		void onResult(T data);
		void onError(String message);
	}

	public void networkCall(ResultHandler<Bitmap> resultHandler) {
		if (true) {
			resultHandler.onError("Error");
		} else {
			Bitmap bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ALPHA_8);
			resultHandler.onResult(bitmap);
		}
	}
}
