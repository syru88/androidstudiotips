package com.marcel.studiotips.mvp;

import android.graphics.Bitmap;


/**
 * Created by marcel on 28.1.2016.
 */
public interface LoginContract {
	interface View {
		void showPicture(Bitmap bitmap);
	}


	interface Presenter {
		void login();
	}
}
