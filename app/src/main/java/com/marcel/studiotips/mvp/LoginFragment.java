package com.marcel.studiotips.mvp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.ImageView;


/**
 * Created by marcel on 28.1.2016.
 */
public class LoginFragment extends Fragment implements LoginContract.View {

//	just for commit


	private LoginContract.Presenter mPresenter;

//	other things to compare


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new LoginPresenter(this);
		mPresenter.login();
	}


	@Override
	public void showPicture(Bitmap bitmap) {
		ImageView imageView = new ImageView(getContext());
		imageView.setImageBitmap(bitmap);
	}
}
