package com.marcel.studiotips.mvp;

import android.graphics.Bitmap;


/**
 * Created by marcel on 28.1.2016.
 */
public class LoginPresenter implements LoginContract.Presenter {

	private LoginContract.View mView;


	public LoginPresenter(LoginContract.View view) {
		mView = view;
	}


//	for git
//	ole


	@Override
	public void login() {
//		Ctrl+Shift+T - generate test
		UserRepository repository = new UserRepository();
		repository.networkCall(new UserRepository.ResultHandler<Bitmap>() {
			@Override
			public void onResult(Bitmap data){
				mView.showPicture(data);
			}


			@Override
			public void onError(String message) {

			}
		});
	}
}
