package com.marcel.studiotips;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

	public ImageView mImageView;
	public String mDummyString;

	private int mUnusedInt;

	// Radek: comments

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//sdffdsdsf
		// THIS COMMENT IS BETTER
		setContentView(R.layout.activity_main);
		//dd
		int count = 4;
		for (int i = 1; i < count; i++) {
			count += 0.5;
		}
	}


	public void intro() {
//		resources
// 		https://www.youtube.com/watch?v=eq3KiAH4IBI
//		https://www.youtube.com/watch?v=Y2GC6P5hPeA
//		IntelliJ IDEA Default Keymap - or Help -> Default Keymap Reference


//		plugins - ADB Idea, Android Parceable, ButterKnife Zelezny, GenyMotion, Fabric, .ignore
//		GsonFormat, Presentation Assistant

//		settings - AltGr+S - CamelHumps, colors of logcat, autoscroll from source code
// 		don't use tabs (Ctrl+Tab, Ctrl+---E, Shift+F4), hide navigation bar
//		optimize imports on the fly
//		TRY TO NOT USE MOUSE!!!

//		Ctrl+Shift+A - find action
//		Ctrl+` - quick switch
	}


	public void toolWindows() {
//		Alt+Number - tool window
//		Ctrl+Tab - number for window
//		Shift+Esc - close current tool window
//		Ctrl+Shift+F12 - show/restore all windows
//		Shift+Ctrl+Arrows - change size of tool window
//		Alt+Arrows - in project window - change views
//		Alt+F12 - open terminal

//		Alt+Shift+Arrows - show next tab in multitab - xml for design
	}


	public void openFile() {
//		Ctrl+N - open class
//		Ctrl+Shift+N - open file
//		Ctrl+Shift+Alt+N - search everywhere (2xShift) - Tab for jumping - "onCreate"
//		\layou - open directory
//		-cs\ str - open file in directory
//		Alt+Insert - create activity
	}


	private void moveInFile() {
//		Ctrl+F12 - file structure
//		Alt+Up/Down - move between methods
//		Ctrl+PgUp/PgDn - move caret to Up/Down of page
//		Ctrl+Up/Down - scroll
	}


	private void editFile() {
//		AltGr+L - reformat code - dialog
//		AltGr+O - organize imports - not necessary - can be done during commit
		/*Ctrl+Space - code completion - getResources*/
		String test = getResources().getString(R.string.test_string);

//		Ctrl+Shift+Space - smart code completion
//		Ctrl++/- - expand/collapse selection; with Shift - whole file
//		Ctrl+D - duplicate line
//		Ctrl+Y - remove line

//		Ctrl+Arrows - jump to previous word - select with Shift
//		Just some sampleText which I Humps can be painful



//		Alt+Insert - generate in context - toString, constructor, etc
		/*Ctrl+Shift+Enter - complete current statement - "if with int"*/
		int b = 30;


//		Ctrl+/ - (un)comment line; block comment with Shift

//		AltGr+T - Surround with

		int a = 10;
		String hello = "Hello";

//		Ctrl+C - copy line when nothing is selected
//		Ctrl+Shift+V - copy from buffer
//		Use some clipboard manager (Ditto)

//		Shift+Ctrl+Arrows - move line - Shift+Alt+Arrows - move line to somewhere else

	}


	private void showHelp() {
//		Ctrl+P - show parameters
		Snackbar.make(mImageView, R.string.app_name, Snackbar.LENGTH_LONG).show();
//		Ctrl+(Shift)+W - extend (shrink) selection
//		Ctrl+B - navigate to declaration
//		F4 - go to source
//		Ctrl+Q - quick documentation
//		Ctrl+Shift+I - show implementation

//		Shift+F1 - show external documentation
		String test = getString(R.string.test_other_string);
	}


	private void mvp() {
//		AltGr+F7 - show usages
// 		Ctrl+Alt+B - show implementation
//		Ctrl+U - go to super method

//		Analyze flow to here on bitmap in LoginFragment
//		Ctrl+Alt+Arrows - navigate back/forward
	}


	private void liveTemplates() {
//		settings Live Templates
//		Ctrl+J - live template
		List<String> array = new ArrayList<>();
		for(int i = 0; i < array.size(); i++) {

		}
		if(array != null) {

		}
		/* .notnull, .for, .cast, etc. */
//		psf, psfs, key, tag, etc.
	}

	private void otherStuff() {
//		inject json
		String json = "{\"name\", \"marcel\"}";
		String jsonInjected = "{\"name\":\"Marcel\"}";
//		Ctrl+F4 - close window

//		tab - complete with Tab
		String complete = absolutelyDifferentName("hello");
//		Ctrl+F1 - show error description
//		Alt+Enter - fix it
//		F2 - show next error - previous error with Shift

//		AltGr+Shift+T - refactor menu
//		Shift+F6 - rename
//		Shift+Ctrl+T - generate test - go to LoginPresenter
//		Alt+J - select next word - private

//		tools in XML
//		Analyze stacktrace

	}


	private void git() {
//		Alt+` - VCS operations popup
//		Alt+9 - open window
//		Alt+Arrows - move between tabs
//		Ctrl+D - show diff
//		show annotaion
//		Ctrl+K - commit
//		Ctrl+Shift+K - push commits
	}


	private String testMethodWithLongName(String name) {
		return name;
	}


	private String absolutelyDifferentName(String name) {
		return name;
	}

	private void getNull() {
		return;
	}

	private void m_masterBla() {
//		master
	}

	public void commityDoRadkovoVetve(Map<Integer, String> aaa) throws IllegalStateException {
		// commituju...
		String retez = new String();

		int a = 20;
		int b = 30;

		int c = a + b;

		throw new IllegalStateException("ERROR");
	}
}
